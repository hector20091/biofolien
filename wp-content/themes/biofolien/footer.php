    <footer class="shop-footer">
        <div class="main-width">
            <div class="row">
                <div class="col-sm-8">
                    <h3>Kontaktieren Sie uns für ein Angebot:</h3>
                    <p>Telefon</p>
                    <h4>+43 664 144 273 6</h4>
                    <p>E-Mail</p>
                    <h4><a href="mailto:office@biofolien.at">office@biofolien.at</a></h4>
                    <div class="footer-nav">
                        <ul>
                            <li><a href="#">Kontaktformular</a></li>
                            <li><a href="#">Rückrufformular</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h3>Adresse</h3>
                    <p>Stermitz Verpackungen <br>
                        Gewerbepark 2 <br>
                        9131 Grafenstein
                    </p>
                    <br>
                    <div class="footer-nav">
                        <ul>
                            <li><a href="#">In Google Maps öffnen</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="line-bottom">
        <div class="main-width">
            <div class="row">
                <div class="col-xs-4 col-sm-6 col-md-8">
                    <div class="copyright">&copy; <?php echo date('Y'); ?>, Biofolien</div>
                </div>
                <div class="col-xs-8 col-sm-6 col-md-4">

                    <?php wp_nav_menu(array(
                        'theme_location' => 'footer',
                        'container_class' => 'footer-nav'
                    )); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
