<?php /**
 * Template Name: Werkzeuge Categories
 * @package WordPress
 * @subpackage ponta.at
 */
 get_header(); ?>
 <div class="wrap-breadcrumbs">
   <div class="main-width">
       <?php breadcrumbs_trail(); ?>
   </div>
 </div>
 <section class="section-product">
   <div class="main-width">
     <?php if(get_field('heading')):?>
       <h1><?php the_field('heading');?></h1>
    <?php else: ?>
      <h1><?php the_title();?></h1>
    <?php endif; ?>
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
         <?php the_content(); ?>
     <?php endwhile; endif; ?>
     <?php /*
     <div class="row products">
       <?php
 			$terms = get_terms(array(    'hide_empty' => false, 'taxonomy' => 'tools_cat','parent'   => 0) );
 			$count = count($terms); $i=0;
 			if ($count > 0) {
 				foreach ($terms as $term) { ?>
          <?php $image = get_field('icon','product_cat_'.$term->term_id); ?>
          <div class="col-lg-2 col-md-4 col-6">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>werkzeuge/kategorie/<?php echo $term->slug;?>/" class="img-product" style="background-image: url(<?php echo $image;?>);"></a>
            <p><a href="<?php echo esc_url( home_url( '/' ) ); ?>werkzeuge/kategorie/<?php echo $term->slug;?>/"><?php echo $term->name; ?></a></p>
          </div>
 					<?php
 				}
 			} ?>
     </div>
     */?>
     <div class="row">
       <div class="col-sm-6">
         <ul class="folder-item">
           <?php
     			$terms = get_terms(array(    'hide_empty' => false, 'taxonomy' => 'tools_cat','parent'   => 0) );
     			$count = count($terms); $i=0;
     			if ($count > 0) {
     				foreach ($terms as $term) { ?>
              <?php $image = get_field('icon','product_cat_'.$term->term_id); ?>
              <li>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>werkzeuge/kategorie/<?php echo $term->slug;?>/">
                  <i class="icon icon-folder"></i>
                  <?php echo $term->name; ?>
                </a>
              </li>
     					<?php
     				}
     			} ?>
         </ul>
       </div>
     </div>

   </div>
 </section>
 <section class="main-content">
 <div class="main-width">
         <div class="row">
           <div class="col-md-6">
             <div class="catalog-info">
               <div class="product-info-img-label">Nicht fündig geworden?</div>
               <p>Ein Katalog kann niemals alles beinhalten, falls Sie nicht fündig geworden sind, fragen Sie bitte bei uns an.</p>
               <p>Ewald Kolar<br><strong>E-Mail:</strong> <a href="mailto:office@eko.at">office@eko.at</a><br><strong>Hotline:</strong> +43 4274 41600</p>
             </div>
           </div>
         </div>
 </div>
 </section>
 <?php get_footer(); ?>
