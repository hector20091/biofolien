<?php
/**
 * @package WordPress
 * @subpackage ponta.at
 */
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="theme-color" content="#ffffff">

    <?php wp_head();?>

</head>
<body <?php body_class(); ?>>
<div class="wrapper shop-page">
    <header class="main-header">
        <div class="main-width">
            <div class="row">
                <div class="nav-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <a href="<?php bloginfo('url'); ?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" alt=""></a>
                <nav class="main-nav">
                    <ul>

                        <?php
                        wp_nav_menu( array(
                                'theme_location'       => 'header-menu',
                                'depth'      => 1,
                                'container'  => false,
                                'items_wrap' => '%3$s',
                                'menu_class' => '',
                            )
                        );
                        ?>

                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <?php if(!is_front_page()) {
        get_template_part('templates/sections/page-hero');
    } ?>
