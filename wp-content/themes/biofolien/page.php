<?php
/**
 * @package WordPress
 * @subpackage ponta.at
 */
 get_header(); ?>

    <section class="content">

        <?php while (have_posts()) : the_post(); ?>

            <div class="main-width">

                <?php the_content(); ?>

            </div>

        <?php endwhile; ?>

    </section>

<?php get_footer(); ?>
