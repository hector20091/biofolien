<?php /**
 * Template Name: Order
 * @package WordPress
 * @subpackage ponta.at
 */
 get_header(); ?>
 <section class="main-content">
   <div class="main-width">
     <h1><?php the_title();?></h1>

     <div class="row contact-block">
       <div class="col-sm-6">
         <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
             <?php the_content(); ?>
         <?php endwhile; endif; ?>
         <div class="row">
           <div class="col-sm-12">
             <?php echo do_shortcode('[contact-form-7 id="1107" title="Online-Bestellschein"]'); ?>
           </div>
         </div>

       </div>

     </div>


   </div>
 </section>
 <?php get_footer(); ?>
