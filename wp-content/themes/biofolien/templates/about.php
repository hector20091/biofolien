<?php /**
 * Template Name: About Us
 * @package WordPress
 * @subpackage ponta.at
 */
 get_header(); ?>
 <?php $i = 1; if( have_rows('sections') ): while ( have_rows('sections') ) : the_row(); ?>
   <?php if ($i % 2 == 0): ?>
     <section class="block-style2 bg-light-blue">
       <div class="main-width">
         <div class="row">
           <div class="col-md-6">
             <?php the_sub_field('text'); ?>
           </div>
           <div class="col-md-6 order-first">
             <img src="<?php the_sub_field('image'); ?>" class="img-full-height">
           </div>
         </div>
       </div>
     </section>
   <?php else: ?>
     <section class="block-style bg-gray">
       <div class="main-width">
         <div class="row">
           <div class="col-md-6">
             <?php the_sub_field('text'); ?>
           </div>
           <div class="col-md-6">
             <img src="<?php the_sub_field('image'); ?>">
           </div>
         </div>
       </div>
     </section>
   <?php endif; $i++;?>
<?php endwhile; endif; ?>
 <?php get_footer(); ?>
