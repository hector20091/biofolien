<?php get_header(); ?>

<section class="content">
    <div class="main-width">

        <?php echo apply_filters('the_content', get_the_archive_description()); ?>

        <?php get_template_part('templates/parts/category-view'); ?>

    </div>
</section>

<?php get_footer(); ?>
