<?php $section = $args['section'];

$title = $section['title'];
$advantages_list = $section['advantages_list'];
$button_title = $section['button_title'];
$button_url = $section['button_url']; ?>

<section class="shop-info-section section-margin">
    <div class="main-width">
        <div class="infoblock">
            <div class="row">

                <?php if(!empty($title)) { ?>

                    <div class="col-sm-6">
                        <h3><?php echo $title; ?></h3>
                    </div>

                <?php } ?>

                <div class="col-sm-6">

                    <?php if(!empty($advantages_list)) { ?>

                        <ul>

                            <?php foreach ($advantages_list as $item) { ?>

                                <li><i>&#10004;</i><?php echo $item['description']; ?></li>

                            <?php } ?>

                        </ul>

                    <?php } ?>

                    <?php if(!empty($button_title) && !empty($button_url)) { ?>

                        <a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_title; ?></a>

                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>