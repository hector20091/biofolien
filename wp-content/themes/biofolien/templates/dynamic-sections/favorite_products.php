<?php global $post;

$section = $args['section'];

$title = $section['title'];
$prodcuts_list = $section['prodcuts_list'];
$button_title = $section['button_title'];
$button_url = $section['button_url']; ?>

<section class="content">
    <div class="main-width">

        <?php if(!empty($title)) { ?>

            <h2><?php echo $title; ?></h2>

        <?php } ?>

        <?php if(sizeof($prodcuts_list) > 0) { ?>

            <div class="carousel-section">
                <div class="owl-carousel carousel-v1">

                    <?php foreach ($prodcuts_list as $post) { setup_postdata($post); ?>

                        <a class="item" href="<?php the_permalink(); ?>">
                            <div class="img-bg" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/mulchfolie-folitec.jpg)" ></div>
                            <div class="title"><?php the_title(); ?></div>
                            <div class="descr">30 my, aus Mater-Bi</div>
                        </a>

                    <?php } wp_reset_postdata(); ?>

                </div>
            </div>

        <?php } ?>

        <?php if(!empty($button_title) && !empty($button_url)) { ?>

            <div class="text-center">
                <a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_title; ?></a>
            </div>

        <?php } ?>

    </div>
</section>