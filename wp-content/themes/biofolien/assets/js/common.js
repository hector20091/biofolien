$(function() {

	function scrollFunction() {
		if( $(window).scrollTop() > 30 ) {
			$('.main-header').addClass('active');
		}else {
			$('.main-header').removeClass('active');
		}
	}
	scrollFunction();
	$(window).scroll(function(){
		scrollFunction();
	});

	$('.nav-icon').click(function(){
		$('html').toggleClass('open-menu');
	});

	$('.carousel-v1').owlCarousel({
		loop: true,
		nav: false,
		margin: 30,
		navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		responsive: {
			0:{
				items: 1
			},
			480:{
				items: 2
			},
			600:{
				items: 3				
			},
			768:{
				items: 3
			},
			992:{
				items: 4,
				nav: true
			}
		}
	})

});
