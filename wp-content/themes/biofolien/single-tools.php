<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Options Framework Theme
 */
 get_header(); ?>
 <div class="wrap-breadcrumbs">
   <div class="main-width">
     <?php breadcrumbs_trail(); ?>
   </div>
 </div>
 <section class="main-content">
   <div class="main-width">
     <?php if(get_field('heading')):?>
       <h1><?php the_field('heading');?></h1>
    <?php else: ?>
      <h1><?php the_title();?></h1>
    <?php endif; ?>

     <section class="section-product-info">
       <div class="row">
           <div class="col-md-12">
             <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
               <?php the_content(); ?>
             <?php endwhile; endif; ?>
           </div>
         </div>
         <div class="section-separator"></div>
        <div class="row">
         <?php if(get_field('product_image')):?>
         <div class="col-md-4 col-padding-top-images">
           <div class="product-info-img">
             <?php if(get_field('label')):?><div class="product-info-img-label"><?php the_field('label'); ?></div><?php endif; ?>
             <?php echo wp_get_attachment_image( get_field( 'product_image' ), 'full' ); ?>
           </div>
         </div>
        <?php endif; ?>
        <?php if(get_field('gallery')): ?>
        <div class="col-md-4 col-padding-top-images">
          <div class="product-info-img product-info-img-slider">
            <?php if(get_field('gallery_label')):?><div class="product-info-img-label"><?php the_field('gallery_label'); ?></div><?php endif; ?>
            <?php
            $images = get_field('gallery');
            $sizeimg = 'slider-product-img';
            $sizethumb = 'slider-product-thumb';
            $sizefull = 'full';
            if( $images ): ?>
              <section class="slider-for">
                <?php foreach( $images as $image ): ?>
                    <div>
                      <a rel="gallery-1" href="<?php echo wp_get_attachment_url( $image['ID'] ); ?>" class="swipebox">
                        <?php echo wp_get_attachment_image( $image['ID'], $sizeimg ); ?>
                      </a>
                    </div>
                <?php endforeach; ?>
              </section>
            <?php endif; ?>
            <?php if( $images ): ?>
              <section class="slider-nav">
                <?php foreach( $images as $image ): ?>
                    <div class="slider-thumbnail">
                      <?php echo wp_get_attachment_image( $image['ID'], $sizethumb ); ?>
                    </div>
                <?php endforeach; ?>
              </section>
            <?php endif; ?>
          </div>
        </div>
        <?php endif; ?>
        <?php if(get_field('technical_image')):?>
        <div class="col-md-4 col-padding-top-images">
          <div class="product-info-img">
            <?php if(get_field('technical_label')):?><div class="product-info-img-label"><?php the_field('technical_label'); ?></div><?php endif; ?>
            <?php echo wp_get_attachment_image( get_field( 'technical_image' ), 'full' ); ?>
          </div>
        </div>
       <?php endif; ?>
       </div>
       <div class="section-separator"></div>


     </section>


      <div class="wrap-table">
 </div>


     <p class="text-right">Letztes Update: <?php the_modified_date('j. F  Y'); ?></p>
     <div class="row">
       <div class="col-md-6">
         <div class="catalog-info">
           <div class="product-info-img-label">Nicht fündig geworden?</div>
           <p>Ein Katalog kann niemals alles beinhalten, falls Sie nicht fündig geworden sind, fragen Sie bitte bei uns an.</p>
           <p>Ewald Kolar<br><strong>E-Mail:</strong> <a href="mailto:office@eko.at">office@eko.at</a><br><strong>Hotline:</strong> +43 4274 41600</p>
         </div>
       </div>
     </div>

   </div>
 </section>

 <script>
jQuery(document).ready(function($) {
 $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  arrows: false,
  dots: false,
  centerMode: true,
  focusOnSelect: true
});
$( '.swipebox' ).swipebox({
		hideBarsDelay : 0,
		loopAtEnd: true
	});
    });
    jQuery(function(){
    jQuery(document.body)
    .on('click touchend','#swipebox-slider .current img', function(e){
    return false;
    })
    .on('click touchend','#swipebox-slider .current', function(e){
    jQuery('#swipebox-close').trigger('click');
    });
    });
 </script>
<?php get_footer(); ?>
