<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="wrap-breadcrumbs">
  <div class="main-width">
      <?php breadcrumbs_trail(); ?>
  </div>
</div>
<?php
$term = get_queried_object();
$parent = $term->parent;
$current_cat = $term->slug;
$current_ID = $term->term_id;
?>
<section class="section-product">
  <div class="main-width">
    <h1><?php echo $term->name; ?></h1>
    <?php $mainterm = $term->term_id; ?>
    <?php
    $children = get_terms( $term->taxonomy, array(
    'parent'    => $term->term_id,
    'hide_empty' => false
    ) );
    ?>

<?php if($children):?>
          <?php foreach ($children as $term) { ?>
            <h3><?php echo $term->name; ?></h3>
            <?php
            $products = get_posts(array(
              'post_type' => 'tools',
              'numberposts' => -1,
              'tax_query' => array(
                array(
                  'taxonomy' => 'tools_cat',
                  'field' => 'id',
                  'terms' => $term->term_id
                )
              )
            ));


            if($products):
            ?>
<div class="row products">





                            <?php foreach ( $products as $post ) : setup_postdata( $post ); ?>
                              <div class="col-lg-2 col-md-4 col-6">
                                <?php
                                $image = wp_get_attachment_image_src( get_field( 'product_image' ),'medium' );
                                 ?>
                                <a href="<?php the_permalink(); ?>" class="img-product" style="background-image: url(<?php echo $image[0]; ?>);"></a>
                                <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                              </div>
                            <?php endforeach; wp_reset_postdata(); ?>

                </div>

            <?php endif; ?>
            <?php }  ?>






                          <?php else:?>
                            <?php
                            $productsmain = get_posts(array(
                              'post_type' => 'tools',
                              'numberposts' => -1,
                              'tax_query' => array(
                                array(
                                  'taxonomy' => 'tools_cat',
                                  'field' => 'id',
                                  'terms' => $mainterm
                                )
                              )
                            ));?>
                            <div class="row products">
                                                        <?php foreach ( $productsmain as $post ) : setup_postdata( $post ); ?>
                                                          <div class="col-lg-2 col-md-4 col-6">
                                                            <?php
                                                            $image = wp_get_attachment_image_src( get_field( 'product_image' ),'medium' );
                                                             ?>
                                                            <a href="<?php the_permalink(); ?>" class="img-product" style="background-image: url(<?php echo $image[0]; ?>);"></a>
                                                            <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                                                          </div>
                                                        <?php endforeach; wp_reset_postdata(); ?>

                                            </div>
                                            <?php endif;?>
      </div>

</section>
<section class="main-content">
<div class="main-width">
        <div class="row">
          <div class="col-md-6">
            <div class="catalog-info">
              <div class="product-info-img-label">Nicht fündig geworden?</div>
              <p>Ein Katalog kann niemals alles beinhalten, falls Sie nicht fündig geworden sind, fragen Sie bitte bei uns an.</p>
              <p>Ewald Kolar<br><strong>E-Mail:</strong> <a href="mailto:office@eko.at">office@eko.at</a><br><strong>Hotline:</strong> +43 4274 41600</p>
            </div>
          </div>
        </div>
</div>
</section>
<?php get_footer(); ?>
