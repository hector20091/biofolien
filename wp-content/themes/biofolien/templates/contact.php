<?php /**
 * Template Name: Contact
 * @package WordPress
 * @subpackage ponta.at
 */
 get_header(); ?>
 <section class="main-content">
   <div class="main-width">
     <h1><?php the_title();?></h1>

     <div class="row contact-block">
       <div class="col-sm-5">
         <h4>Firmensitz</h4>
         <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
             <?php the_content(); ?>
         <?php endwhile; endif; ?>

         <p>
           <span class="contact-width">Tel:</span> <a href="tel:+43 4274 41600"><b>+43 4274 41600</b></a> <br>
           <span class="contact-width">Fax:</span> <b>+43 4274 41600 89</b> <br>
           <span class="contact-width">E-mail:</span> <a href="mailto:office@eko.at"><b>office@eko.at</b></a>
         </p>
         <h4>Ihr Ansprechpartner</h4>

         <div class="row ihr_ansprechpartner">

           <div class="col-7">
             <h4>Ewald Kolar</h4>
             <p>Geschäftsführer + Vertrieb</p>
             <p>Mobil: <a href="tel:+43 664 1923 600">+43 664 1923 600</a> <br>
             E-mail: <a href="mailto:kolar@eko.at">kolar@eko.at</a></p>
           </div>
           <div class="col-5">
             <img src="<?php bloginfo('template_directory');?>/images/kontakt.jpg">
           </div>
         </div>
       </div>
       <div class="col-sm-7">
         <h4>Hinterlassen Sie uns eine Nachricht</h4>
         <?php echo do_shortcode('[contact-form-7 id="440" title="Contact"]'); ?>
       </div>
     </div>


   </div>
 </section>
 <?php get_footer(); ?>
