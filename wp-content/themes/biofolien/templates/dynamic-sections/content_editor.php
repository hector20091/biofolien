<?php $section = $args['section'];

$content = $section['content']; ?>

<section class="content">
    <div class="main-width">

        <?php echo $content; ?>

    </div>
</section>