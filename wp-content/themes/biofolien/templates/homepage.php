<?php /**
 * Template Name: Homepage
 * @package WordPress
 * @subpackage ponta.at
 */
 get_header(); ?>
 <section class="block-style bg-dark-grey">
   <div class="main-width">
     <div class="row">
       <div class="col-md-5">
         <div class="hero-text">
           <?php the_field('hero_text'); ?>
         </div>
         <p><a href="<?php the_field('button_link');?>" class="btn">MEHR ÜBER EKO</a></p>
       </div>
       <div class="col-md-6 offset-1">
         <div class="slider">
           <?php if( have_rows('slider') ): while ( have_rows('slider') ) : the_row(); ?>
             <div>
               <?php echo wp_get_attachment_image( get_sub_field('image'), 'slider-image' );  ?>
               <p><?php the_sub_field('text'); ?></p>
             </div>
          <?php endwhile; endif; ?>
         </div>
       </div>
     </div>
   </div>
 </section>
 <section class="block-style bg-light-gr">
   <div class="main-width">
     <div class="row">
       <div class="col-md-6">
         <img src="<?php bloginfo('template_directory');?>/images/online-catalog.png">
       </div>
       <div class="col-md-6">
         <?php the_field('catalog_text'); ?>
         <p><a href="<?php echo esc_url( home_url( '/produkte/' ) ); ?>" class="btn">ZUM ONLINE KATALOG</a></p>
       </div>
     </div>

   </div>
 </section>
 <?php /*
 <section class="block-style bg-light-green">
   <div class="main-width">
     <h2 class="text-center">EKO Industriebedarf...  <br> gut verbunden!</h2>
   </div>
 </section>
 */?>
 <?php get_footer(); ?>
