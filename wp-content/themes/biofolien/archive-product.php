<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<!-- content -->
<section class="content-container">
		<div class="container">
				<div class="row">
						<div class="col-xs-12 col-md-8 col-lg-9">

								<div class="gallery-container">
									<h2 class="page-heading">
											Bridge to Bridge Photo Gallery 4
									</h2>

										<div id="lightgallerypage" class="gallery-photos clearfix">
												<?php
												 $gallery = new WP_Query(array('post_type'=>'gallery','posts_per_page' => -1));
												?>
												 <?php if( $gallery->have_posts() ): ?>
														 <?php while ($gallery->have_posts() ): $gallery->the_post(); ?>
															 <?php if( get_field('item_type') == 'video' ): ?>
															 <a class="lg-a" href="<?php the_field('youtube_link'); ?>">
															 <?php else: ?>
															 <a class="lg-a" href="<?php the_post_thumbnail_url('full'); ?>">
															 <?php endif; ?>
																	 <img src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php the_title(); ?>">
															 </a>
														 <?php endwhile;?>
												 <?php endif; ?>
												 <?php wp_reset_postdata(); ?>

										</div>


								</div>

						</div>

						<?php get_sidebar(); ?>

				</div>
		</div>
</section>
<?php get_footer(); ?>
