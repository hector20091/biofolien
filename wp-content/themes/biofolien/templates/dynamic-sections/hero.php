<?php $section = $args['section'];

$bg_url = $section['background'];
$content = $section['description']; ?>

<section class="hero-section" style="background-image: url(<?php echo $bg_url; ?>);">
    <div class="main-width">
        <div class="row">
            <div class="col-lg-6 col-sm-8">

                <?php echo $content; ?>

            </div>
        </div>
    </div>
</section>