<?php global $wp_query;

$parent_id = 0;
$args = array(
    'taxonomy'   => 'product_cat',
    'hide_empty' => true
);

if(is_archive()) {
    $term = get_queried_object();

    $parent_id = $term->term_id;
}

$args['parent'] = $parent_id;

$categories = get_terms($args); ?>

<div class="category">

    <?php if(sizeof($categories) > 0) { ?>

        <div class="row">

            <?php foreach ($categories as $cat) { ?>

                <div class="col-md-4 col-sm-6">
                    <a href="<?php echo get_term_link($cat); ?>" class="product">
                        <div class="product-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/prod-1.png" alt="" class="img-responsive">
                        </div>
                        <div class="product-footer">
                            <h4><?php echo $cat->name; ?></h4>
                            <div class="descr"><?php echo wp_trim_words($cat->description,10); ?></div>
                        </div>
                    </a>
                </div>

            <?php } ?>

        </div>

    <?php } else { ?>

        <p>Nothing Found!</p>

    <?php } ?>

</div>