<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => 'B2B',
  'menu_type'       => 'menu', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'cs-framework',
  'ajax_save'       => false,
  'show_reset_all'  => false,
  'framework_title' => 'Blog Theme Settings <small>by Webtaculos</small>',
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();

// ----------------------------------------
// a option section for options overview  -
// ----------------------------------------
$options[]      = array(
  'name'        => 'announcement',
  'title'       => 'Announcement',
  'icon'        => 'fa fa-star',

  // begin: fields
  'fields'      => array(
    array(
      'id'      => 'announcement_status',
      'type'    => 'switcher',
      'title'   => 'Active',
      'label'   => 'Set if the announcement is active or not.',
    ),
    array(
      'id'      => 'announcement_text',
      'type'    => 'text',
      'title'   => 'Text',
    ),
    array(
      'id'      => 'announcement_link',
      'type'    => 'text',
      'title'   => 'Link',
    ),






  ), // end: fields
);
$options[]      = array(
  'name'        => 'viewpackage',
  'title'       => 'View Package',
  'icon'        => 'fa fa-file-pdf-o',

  // begin: fields
  'fields'      => array(
    array(
      'id'      => 'packagepdf',
      'type'    => 'upload',
      'title'   => 'PDF File',
      'help'    => 'Upload pdf file for view package.',
    ),







  ), // end: fields
);




// ------------------------------
// backup                       -
// ------------------------------
$options[]   = array(
  'name'     => 'backup_section',
  'title'    => 'Backup',
  'icon'     => 'fa fa-shield',
  'fields'   => array(

    array(
      'type'    => 'notice',
      'class'   => 'warning',
      'content' => 'You can save your current options. Download a Backup and Import.',
    ),

    array(
      'type'    => 'backup',
    ),

  )
);










CSFramework::instance( $settings, $options );
