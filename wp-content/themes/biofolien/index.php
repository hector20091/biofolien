<?php
/**
 * The main template file.
 *
 * This theme is purely for the purpose of testing theme options in Options Framework plugin.
 *
 * @package WordPress
 * @subpackage Options Framework Theme
 */
 get_header(); ?>
 <!-- content -->
 <section class="content-container">
     <div class="container">
         <div class="row">
             <div class="col-xs-12 col-md-8 col-lg-9">

                 <div class="news-container">
                   <h2 class="page-heading">
                       Bridge to Bridge News
                       <span>The latest news &amp; updates from Waterski NSW</span>
                   </h2>
                     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                       <div class="news-content-container">
                           <h3><?php the_title(); ?></h3>
                           <span>Posted on <?php the_time('j F Y') ?></span>
                           <div class="news-content clearfix">
                               <?php the_content(); ?>
                           </div>
                       </div>
                     <?php endwhile; endif; ?>

                     <?php creatio_pagination(); ?>

                 </div>

             </div>

             <?php get_sidebar(); ?>

         </div>
     </div>
 </section>
<?php get_footer(); ?>
