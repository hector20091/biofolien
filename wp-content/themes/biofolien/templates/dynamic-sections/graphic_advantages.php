<?php $section = $args['section'];

$title = $section['title'];
$advantages_list = $section['advantages_list']; ?>

<section>
    <div class="main-width">

        <?php if(!empty($title)) { ?>

            <h3><?php echo $title; ?></h3>

        <?php } ?>

        <?php if(!empty($advantages_list)) { ?>

            <ul class="item-icons">

                <?php foreach ($advantages_list as $item) {
                    $image = $item['image'];
                    $title = $item['title'];
                    $description = $item['description']; ?>

                    <li>
                        <div class="item-icon"><img src="<?php echo $image; ?>"></div>
                        <h4><?php echo $title; ?></h4>

                        <?php echo $description; ?>

                    </li>

                <?php } ?>

            </ul>

        <?php } ?>

    </div>
</section>