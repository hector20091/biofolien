<?php /**
 * Template Name: Search
 * @package WordPress
 * @subpackage ponta.at
 */
 get_header(); ?>
 <section class="main-content">
   <div class="main-width">
     <?php /*
     <form class="search-long">
       <button type="submit"><i class="icon icon-search"></i></button>
       <input type="text" class="form-control" id="tags_1">
     </form>
     */?>
     <p class="search-results"><?php echo $wp_query->found_posts;?> search results</p>
     <?php
 		if ( have_posts() ) :?>
    <ul class="search-item">
      <?php	while ( have_posts() ) : the_post(); ?>
        <li>
          <a href="<?php the_permalink();?>" class="img-product" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field( 'product_image' ), 'full' ); ?>);"></a>
          <p><a href="<?php the_permalink();?>"><b><?php if(get_field('heading')):?><?php the_field('heading');?><?php else: ?><?php the_title();?><?php endif; ?></b></a> <?php if(get_post_type( get_the_ID() ) == 'tools' || get_post_type( get_the_ID() ) == 'products'):?>in <a href="<?php echo esc_url( home_url( '/produkte/' ) ); ?>">produkte</a>
             page</p><?php endif; ?>
        </li>
      <?php endwhile; // End of the loop.?>
      <?php echo creatio_pagination(); ?>
<?php	else : ?>

 			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
 			<?php
 		endif;
 		?>
     </ul>

   </div>
 </section>
 <?php get_footer(); ?>
