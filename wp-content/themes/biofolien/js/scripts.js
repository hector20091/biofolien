jQuery(document).ready(function($) {


	$('.slider').slick({
	  dots: true,
	  infinite: true,
	  arrows: true,
	  speed: 1500,
  autoplay: true,
  autoplaySpeed: 5000,
	});

	$('.button-menu').click(function(){
		$('html').toggleClass('open-menu');
	});





	jQuery(".img_anim").after( function() {

	  // keep ref to the image
		var image = this;

		// add div container for the link
		var $d = jQuery("<div />");

		// add link
		var $a = jQuery("<a class='btn-upload running'><span class='stop-btn'><i class='ico-start'></i> Start</span><span class='start-btn'><i class='ico-stop'></i> Stop</span></a>");
		$d.append($a);

		// add click event
		$a.on("click", function() {

			// get the src of the image
			var src = jQuery(image).attr("src");

			// change the image
			if(jQuery(src.split("_")).last()[0] == "still.jpg"){
				jQuery(image).attr('src', src.replace('_still.jpg', '.gif'));
				jQuery('.btn-upload').addClass('running');
			}else{
				jQuery(image).attr('src', src.replace('.gif', '_still.jpg'));
				jQuery('.btn-upload').removeClass('running');
			}
		})
		return $d;

	});



	$("#print-button").on('click', function() {
		//Print ele4 with custom options
		$("#wrapper").print({

		});
	});

});
