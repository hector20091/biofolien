<?php
/**
 * Plugin Name: Breadcrumbs
 * Plugin URI:  https://themehybrid.com/plugins/breadcrumbs-trail
 * Description: A smart breadcrumb menu plugin embedded with <a href="http://schema.org">Schema.org</a> microdata that can handle variations in site structure more accurately than any other breadcrumb plugin for WordPress. Insert into your theme with the <code>breadcrumbs_trail()</code> template tag.
 * Version:     1.0.0
 * Author:      Justin Tadlock
 * Author URI:  https://themehybrid.com
 * Text Domain: breadcrumbs-trail
 * Domain Path: /lang
 */

# Extra check in case the script is being loaded by a theme.
if ( ! function_exists( 'breadcrumbs_trail' ) )
	require_once( 'inc/breadcrumbs.php' );

# Plugin setup callback.
add_action( 'plugins_loaded', 'breadcrumbs_trail_setup' );

# Check theme support. */
add_action( 'after_setup_theme', 'breadcrumbs_trail_theme_setup', 12 );

/**
 * Plugin setup. For now, it just loads the translation.
 *
 * @since  1.1.0
 * @access public
 * @return void
 */
function breadcrumbs_trail_setup() {

	load_plugin_textdomain( 'breadcrumbs-trail', false, trailingslashit( dirname( plugin_basename( __FILE__ ) ) ) . 'lang' );
}

/**
 * Checks if the theme supports the Breadcrumb Trail script.  If it doesn't, we'll hook some styles
 * into the header.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function breadcrumbs_trail_theme_setup() {

	if ( ! current_theme_supports( 'breadcrumbs-trail' ) )
		add_action( 'wp_head', 'breadcrumbs_trail_print_styles' );
}

/**
 * Prints CSS styles in the header for themes that don't support Breadcrumb Trail.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function breadcrumbs_trail_print_styles() {

}
