<?php $title = get_the_title();

if(is_archive()) {
    $title = get_the_archive_title();
}

$thumbnail_url = get_template_directory_uri() . '/assets/img/AdobeStock_131017066.jpeg';
if(has_post_thumbnail()) {
    $thumbnail_url = get_the_post_thumbnail_url();
}

?>

<section class="hero-section3" style="background-image: url(<?php echo $thumbnail_url; ?>);">
    <div class="main-width">
        <h2><?php echo $title; ?></h2>
    </div>
</section>