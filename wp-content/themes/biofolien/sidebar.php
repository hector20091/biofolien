<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage ctsweep
 */
?>
<div class="col-xs-12 col-md-4 col-lg-3">
		<div class="main-sidebar">
				<div class="sidebar-links">
						<h4>Popular Downloads</h4>
						<?php if( have_rows('sidebar_popular_downloads', 4) ): ?>
						    <?php while( have_rows('sidebar_popular_downloads', 4) ): the_row(); ?>
									<a class="btn btn-primary" href="<?php the_sub_field('link'); ?>" <?php if(get_sub_field('target') == 'blank'):?>target="_blank"<?php endif;?>><?php the_sub_field('text'); ?></a>
						    <?php endwhile; ?>
						<?php endif; ?>
				</div>
				<div class="sidebar-block clearfix">
					<div class="sidebar-inner-block">
							<a href="<?php echo cs_get_option('packagepdf');?>" target="_blank">
									<img src="<?php bloginfo('template_directory');?>/images/img/b2b-package.png" alt="bridge to bridge">
							</a>
					</div>
						<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
								<?php dynamic_sidebar( 'sidebar' ); ?>
						<?php endif; ?>
				</div>

		</div>
</div>
