<?php
//require_once dirname( __FILE__ ) .'/cs-framework/cs-framework.php';
require_once dirname( __FILE__ ) .'/includes/breadcrumbs.php';
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'header-menu-inner' => __( 'Inner Pages Header Menu' ),
      'footer' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );
add_action( 'after_setup_theme', 'hh_theme_setup', 11 );
function hh_theme_setup(){
	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'video' ) );
}

function twentyten_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'twentyten_excerpt_length' );

function truncateString($str, $chars, $to_space) {
   if($chars > strlen($str)) return $str;

   $str = substr($str, 0, $chars);
   $space_pos = strrpos($str, " ");
   if($to_space && $space_pos >= 0)
       $str = substr($str, 0, strrpos($str, " "));

   return($str);
}
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'thumbnail-gallery', 210, 210, true);
  add_image_size( 'slider-product-thumb', 80, 80, true);
  add_image_size( 'slider-product-img', 350, 390, true);
  add_image_size( 'slider-image', 555, 415, true);
}
 function osg_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar', 'osg' ),
		'id' => 'sidebar',
		'before_widget' => '<div class="sidebar-inner-block">',
		'after_widget' => "</div>",
		'before_title' => '',
		'after_title' => '',
	) );
}
add_filter('widget_text', 'do_shortcode');
add_action( 'widgets_init', 'osg_widgets_init' );
function replace_last_nav_item($items, $args) {
  return substr_replace($items, '', strrpos($items, $args->after), strlen($args->after));
}
add_filter('wp_nav_menu','replace_last_nav_item',100,2);

class BTB_Walker extends Walker_Nav_Menu
{

/**
 * @see Walker::start_lvl()
 * @since 3.0.0
 *
 * @param string $output Passed by reference. Used to append additional content.
 * @param int $depth Depth of page. Used for padding.
 */
public function start_lvl(&$output, $depth = 0, $args = array())
{
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\" dropdown\">\n";
}

/**
 * @see Walker::start_el()
 * @since 3.0.0
 *
 * @param string $output Passed by reference. Used to append additional content.
 * @param object $item Menu item data object.
 * @param int $depth Depth of menu item. Used for padding.
 * @param int $current_page Menu item ID.
 * @param object $args
 */
public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
{
    $indent = ($depth) ? str_repeat("\t", $depth) : '';

    /**
     * Dividers, Headers or Disabled
     * =============================
     * Determine whether the item is a Divider, Header, Disabled or regular
     * menu item. To prevent errors we use the strcasecmp() function to so a
     * comparison that is not case sensitive. The strcasecmp() function returns
     * a 0 if the strings are equal.
     */
    if (strcasecmp($item->attr_title, 'divider') == 0 && $depth === 1) {
        $output .= $indent . '<li role="presentation" class="divider">';
    } else if (strcasecmp($item->title, 'divider') == 0 && $depth === 1) {
        $output .= $indent . '<li role="presentation" class="divider">';
    } else if (strcasecmp($item->attr_title, 'dropdown-header') == 0 && $depth === 1) {
        $output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr($item->title);
    } else if (strcasecmp($item->attr_title, 'disabled') == 0) {
        $output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr($item->title) . '</a>';
    } else {

        $class_names = $value = '';

        $classes = empty($item->classes) ? array() : (array)$item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));

        if ($args->has_children)
            $class_names .= ' has-dropdown';

        if (in_array('current-menu-item', $classes))
            $class_names .= ' active';

        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = $id ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '<li' . $id . $value . $class_names . '>';

        $atts = array();
        $atts['title'] = !empty($item->title) ? $item->title : '';
        $atts['target'] = !empty($item->target) ? $item->target : '';
        $atts['rel'] = !empty($item->xfn) ? $item->xfn : '';


        $atts['custom'] = !empty($item->custom) ? $item->custom : '';

        // If item has_children add atts to a.
        if ($args->has_children && $depth === 0) {
            $atts['href'] = '#';
            $atts['data-toggle'] = 'dropdown';
            $atts['class'] = 'dropdown-toggle';
            $atts['aria-haspopup'] = 'true';
        } else {
            $atts['href'] = !empty($item->url) ? $item->url : '';
        }

        $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args);


        $attributes = '';
        foreach ($atts as $attr => $value) {


            if (!empty($value)) {
                $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $item_output = $args->before;

        /*
         * Glyphicons
         * ===========
         * Since the the menu item is NOT a Divider or Header we check the see
         * if there is a value in the attr_title property. If the attr_title
         * property is NOT null we apply it as the class name for the glyphicon.
         */
        if (!empty($item->attr_title))
            $item_output .= '<a' . $attributes . '><span class="glyphicon ' . esc_attr($item->attr_title) . '"></span>&nbsp;';
        else
            $item_output .= '<a' . $attributes . '>';


        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        $item_output .= ($args->has_children && 0 === $depth) ? ' <i class="fa fa-angle-down"></i></a>' : '</a>';
        $item_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
}

/**
 * Traverse elements to create list from elements.
 *
 * Display one element if the element doesn't have any children otherwise,
 * display the element and its children. Will only traverse up to the max
 * depth and no ignore elements under that depth.
 *
 * This method shouldn't be called directly, use the walk() method instead.
 *
 * @see Walker::start_el()
 * @since 2.5.0
 *
 * @param object $element Data object
 * @param array $children_elements List of elements to continue traversing.
 * @param int $max_depth Max depth to traverse.
 * @param int $depth Depth of current element.
 * @param array $args
 * @param string $output Passed by reference. Used to append additional content.
 * @return null Null on failure with no changes to parameters.
 */
public function display_element($element, &$children_elements, $max_depth, $depth, $args, &$output)
{
    if (!$element)
        return;

    $id_field = $this->db_fields['id'];

    // Display this element.
    if (is_object($args[0]))
        $args[0]->has_children = !empty($children_elements[$element->$id_field]);

    parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
}

    function theme_get_template( $template = '', $args = array(), $echo = true ) {
        ob_start();
        include TEMPLATEPATH . '/' . $template . '.php';
        $content = ob_get_contents();
        ob_end_clean();

        if($echo) {
            echo $content;
        } else {
            return $content;
        }
    }

/**
 * Menu Fallback
 * =============
 * If this function is assigned to the wp_nav_menu's fallback_cb variable
 * and a manu has not been assigned to the theme location in the WordPress
 * menu manager the function with display nothing to a non-logged in user,
 * and will add a link to the WordPress menu manager if logged in as an admin.
 *
 * @param array $args passed from the wp_nav_menu function.
 *
 */
public static function fallback($args)
{
    if (current_user_can('manage_options')) {

        extract($args);

        $fb_output = null;

        if ($container) {
            $fb_output = '<' . $container;

            if ($container_id)
                $fb_output .= ' id="' . $container_id . '"';

            if ($container_class)
                $fb_output .= ' class="' . $container_class . '"';

            $fb_output .= '>';
        }

        $fb_output .= '<ul';

        if ($menu_id)
            $fb_output .= ' id="' . $menu_id . '"';

        if ($menu_class)
            $fb_output .= ' class="' . $menu_class . '"';

        $fb_output .= '>';
        $fb_output .= '<li><a href="' . admin_url('nav-menus.php') . '">Add a menu</a></li>';
        $fb_output .= '</ul>';

        if ($container)
            $fb_output .= '</' . $container . '>';

        echo $fb_output;
    }
}
}

function onlinesoftcustom_scripts() {
    wp_deregister_script('jquery');

    wp_register_script('jquery', get_theme_file_uri('/assets/js/jquery-2.2.3.min.js'));
    wp_register_script('bf-scripts', get_theme_file_uri('/assets/js/scripts.js'),array('jquery'),false,true);

    // Scripts
    wp_enqueue_script('jquery');
    wp_enqueue_script('bf-scripts');

    //Styles
    wp_enqueue_style( 'bf-style', get_theme_file_uri('/assets/css/style.css'));
    wp_enqueue_style( 'bf-main', get_theme_file_uri('/assets/css/main.min.css'));
	wp_enqueue_style( 'bf-root-style', get_stylesheet_uri());
}
add_action( 'wp_enqueue_scripts', 'onlinesoftcustom_scripts' );
add_theme_support( 'title-tag' );
// Fix Advanced Custom Fields and Visual Composer CSS conflict
function afc_vc_fix() {
	echo '
	<style>
		.repeater .row:before,
		.repeater .row:after {
			display: auto;
			content: none;
		}
	</style>';
}
add_action('admin_head', 'afc_vc_fix');

function _substr($str, $length, $minword = 3)
{
	$sub = '';
	$len = 0;
	foreach (explode(' ', $str) as $word)
	{
		$part = (($sub != '') ? ' ' : '') . $word;
		$sub .= $part;
		$len += strlen($part);
		if (strlen($word) > $minword && strlen($sub) >= $length)
		{
			break;
		}
	}
	return $sub . (($len < strlen($str)) ? '...' : '');
}






add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});
add_theme_support( 'post-thumbnails' );
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'tools',
    array(
      'labels' => array(
        'name' => __( 'Werkzeuge', 'osg' ),
        'singular_name' => __( 'Werkzeuge', 'osg' ),
        'add_new' => __( 'Add new Werkzeuge', 'osg' )
      ),

    'public' => true,
    'show_ui' => true,
  'rewrite'            => array( 'slug' => 'werkzeuge' ),
    'hierarchical'       => false,
    'menu_position'      => null,
    'capability_type'    => 'post',
    'supports' => array('title', 'thumbnail', 'editor', 'excerpt')
    )
  );

  /**
   * Register Taxonomy Category for post type Recipe
   */
  $labels = array(
    'name'              => __( 'Categories', 'osg' ),
    'singular_name'     => __( 'Category', 'osg' ),
    'search_items'      => __( 'Search Categories', 'osg' ),
    'all_items'         => __( 'All Categories', 'osg' ),
    'parent_item'       => __( 'Parent Category', 'osg' ),
    'parent_item_colon' => __( 'Parent Category:', 'osg' ),
    'edit_item'         => __( 'Edit Category', 'osg' ),
    'update_item'       => __( 'Update Category', 'osg' ),
    'add_new_item'      => __( 'Add New Category', 'osg' ),
    'new_item_name'     => __( 'New Category Name', 'osg' ),
    'menu_name'         => __( 'Categories', 'osg' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'rewrite' => array( 'slug' => 'kategorie' ),
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true
  );

  register_taxonomy( 'tools_cat', array( 'tools' ), $args );




  	register_post_type( 'products',
  		array(
  			'labels' => array(
  				'name' => __( 'Produkte', 'osg' ),
  				'singular_name' => __( 'Product', 'osg' ),
  				'add_new' => __( 'Add new product', 'osg' )
  			),

  		'public' => true,
  		'show_ui' => true,
		'rewrite'            => array( 'slug' => 'produkte' ),
  		'hierarchical'       => false,
  		'menu_position'      => null,
  		'capability_type'    => 'post',
  		'supports' => array('title', 'thumbnail', 'editor', 'excerpt')
  		)
  	);

  	/**
  	 * Register Taxonomy Category for post type Recipe
  	 */
  	$labels = array(
  		'name'              => __( 'Categories', 'osg' ),
  		'singular_name'     => __( 'Category', 'osg' ),
  		'search_items'      => __( 'Search Categories', 'osg' ),
  		'all_items'         => __( 'All Categories', 'osg' ),
  		'parent_item'       => __( 'Parent Category', 'osg' ),
  		'parent_item_colon' => __( 'Parent Category:', 'osg' ),
  		'edit_item'         => __( 'Edit Category', 'osg' ),
  		'update_item'       => __( 'Update Category', 'osg' ),
  		'add_new_item'      => __( 'Add New Category', 'osg' ),
  		'new_item_name'     => __( 'New Category Name', 'osg' ),
  		'menu_name'         => __( 'Categories', 'osg' ),
  	);

  	$args = array(
  		'hierarchical'      => true,
  		'labels'            => $labels,
      'rewrite' => array( 'slug' => 'kategorie' ),
  		'show_ui'           => true,
  		'show_admin_column' => true,
  		'query_var'         => true
  	);

  	register_taxonomy( 'product_cat', array( 'products' ), $args );




    register_post_type( 'legend',
  		array(
  			'labels' => array(
  				'name' => __( 'Legends', 'osg' ),
  				'singular_name' => __( 'Legend', 'osg' ),
  				'add_new' => __( 'Add new Legend', 'osg' )
  			),

  		'public' => true,
  		'show_ui' => true,
  		'has_archive'        => true,
  		'hierarchical'       => false,
  		'menu_position'      => null,
  		'capability_type'    => 'post',
  		'supports' => array('title', 'thumbnail', 'editor', 'excerpt')
  		)
  	);




}




function creatio_pagination($pages = '', $range = 2)
{
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
         echo "<div class='pagination'><ul>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li class='active'><span>".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a></li>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</ul></div>\n";
     }
}
if( function_exists('acf_add_options_page') ) {

 acf_add_options_page();

}
add_filter('body_class','alter_search_classes');
function alter_search_classes($classes) {
    if(is_search()){
       return array();
    } else {
       return $classes;
    }

}


function my_pre_get_posts($query) {

    if( is_admin() )
        return;

    if( is_search() && $query->is_main_query() ) {
        $query->set('post_type', 'products');
    }

}

add_action( 'pre_get_posts', 'my_pre_get_posts' );
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
  .categorydiv div.tabs-panel, .customlinkdiv div.tabs-panel, .posttypediv div.tabs-panel, .taxonomydiv div.tabs-panel, .wp-tab-panel {
  	max-height: 5000px;
  }
  </style>';
}

function theme_get_template( $template = '', $args = array(), $echo = true ) {
    ob_start();
    include TEMPLATEPATH . '/' . $template . '.php';
    $content = ob_get_contents();
    ob_end_clean();

    if($echo) {
        echo $content;
    } else {
        return $content;
    }
}

function list_categories_shortcode($atts) {
    return theme_get_template('templates/parts/list-categories',array(),false);
}
add_shortcode('list_categories','list_categories_shortcode');